<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);

$host        = "host = localhost";//192.168.0.104";
$port        = "port = 5432";
$dbname      = "dbname = Magic";
$credentials = "user = postgres password=postgres";
$dataserver='';
$lands= array(131,422,718,985,1247,88,391,678,960,1224,191,479,777,1030,1286,257,525,818,1114,1340,168,462,753,1015,1270);
$landsTam=25;
$db = pg_connect( "$host $port $dbname $credentials");
if(!$db) {
    die( "Error : Unable to open database");
}


$sql = "SELECT * FROM DECK WHERE IDDONO = 1";

$con = pg_query($db,$sql) or die("Deu errado");


$table = $_GET['id'];

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MTGA WebTool</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="_css/bootstrap.min.css">
    <script src="_js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="_css/index.css">
    <script>
        function activeLine(nLine) {
            var linha = document.getElementById("linhaAtiva");
            if(linha){
                linha.id= "linha"+linha.value;
                linha.setAttribute("onclick","activeLine("+linha.value+")");
                linha.setAttribute("class","");
            }
            linha = document.getElementById("linha"+nLine);
            linha.value = nLine;
            linha.classList.add("active-line");

            linha.id = "linhaAtiva";
            linha.setAttribute("onclick", "");

            var ip = "<?php echo $_SERVER['SERVER_ADDR'];?>";

            var local= "http://{0}/MtgaWebtool/webcontent/?id="+nLine;
            local = local.replace("{0}",ip);
            console.log(local);
            window.location.replace(local);

        }

    </script>
    <style>
        .active-line{
            background: #1c449d !important;
            color: #fff;
        }
    </style>

</head>
<body>
<header class="container">
    <h1>MTGA Deck Constructor Tool</h1>
    <p>Veja qual deck está mais proximo de montar com seu grimório.
</header>


<div class="container">

    <div>
        <h5>Upar Grimorio</h5>
        <form action="../server/grimorioinsert.php" method="post">
            <label for="grimorio">Lista de Cartas: </label>
            <input name="grimorio" type="text" required>

            <input type="submit" value="Enviar">
        </form>
    </div>


    <h5>Insira Decks que deseja montar</h5>
    <form action="../server/deckinsert.php" method="post">
        <label for="nome">Nome do Deck: </label>
        <input name="nome" type="text" required>

        <label for="cards">Lista de Cartas: </label>
        <input name="cards" type="text" required>

        <label for="dono">Usuario</label>
        <input type="radio">
        <input type="submit" value="Enviar">
    </form>

    <div class="pre-scrollable">
        <table  class="table table-sm table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Progresso</th>
            </tr>
            </thead>
            <tbody>
            <?php while($dado = pg_fetch_array($con)){
                $deckid = $dado['id'];
                $sqldeck= "SELECT * FROM DECK_CARD WHERE IDDECK = ".$deckid;
                $sqlgrim = "SELECT G.QTD FROM GRIMORIO as G WHERE G.IDCARD = ";
                $result = pg_query($db,$sqldeck) or die("ERRO!");


                $progress= 0;

                while($decklinha = pg_fetch_array($result)){
                    $idCardDeck=$decklinha['idcard'];
                    $qtdCardDeck=$decklinha['qtd'];

                    $getQtdGrim = pg_query($db,$sqlgrim.$idCardDeck);
                    $getQtdGrim = pg_fetch_array($getQtdGrim);

                    $qtdHave = $getQtdGrim['qtd'];

                    for ($x=0;$x<$landsTam;$x++){
                        if (intval($idCardDeck) === $lands[$x]){
                            $progress += $qtdCardDeck;
                            
                            continue;
                        }
                    }
                    if($qtdCardDeck < $qtdHave){
                        $progress+=$qtdCardDeck;
                    }else{
                        $progress+=$qtdHave;
                    }

                }

                $percent =intval(($progress*100)/60);
                ?>
                <tr value="<?php echo $dado["id"];?>" id="linha<?php echo $dado["id"];?>" onclick="activeLine(<?php echo $dado["id"];?>)">
                    <td><a><?php echo $dado["nome"];?></a></td>
                    <td><a><?php echo "<div class=\"progress\">
                <div class=\"progress-bar\" role=\"progressbar\" style=\"width: $percent%\" aria-valuenow=\"$percent\" aria-valuemin=\"0\" aria-valuemax=\"100\">$percent%</div>
            </div>";?></a></td>
                </tr>

                <?php
            }

            ?>

            </tbody>
        </table>
    </div>
    <div id="deckInfo">
        <?php
            if(!empty($table)) {
                $deckname= "SELECT NOME FROM DECK WHERE ID = ".$table;
                $deckname = pg_query($db,$deckname);
                $deckname = pg_fetch_array($deckname);
                echo "<h3>".$deckname['nome']."</h3>";
            }?>
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th scope="col">CARD NAME</th>
                <th scope="col">QTD</th>
                <th scope="col">QTD</th>
                <th scope="col">CARD NAME</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($table)) {
                $tablesql = "SELECT TB.DECKNAME,TB.DECKCARDNAME,TB.DECKQTD,TB.GRIMCARDNAME,TB.QTD FROM TABELACARDS AS TB WHERE ID = ".$table;
                $result = pg_query($db,$tablesql);
                while($tablelinha = pg_fetch_array($result)){
                    if(intval($tablelinha['deckqtd'])>4){
                        echo"<tr>
                            <td><a>".$tablelinha['deckcardname']."</a></td>
                            <td><a>".$tablelinha['deckqtd']."</a></td>
                            <td><a>".$tablelinha['deckqtd']."</a></td>
                            <td><a>".$tablelinha['deckcardname']."</a></td>
                        </tr>";
                    }else {
                        echo "<tr>
                            <td><a>" . $tablelinha['deckcardname'] . "</a></td>
                            <td><a>" . $tablelinha['deckqtd'] . "</a></td>
                            <td><a>" . $tablelinha['qtd'] . "</a></td>
                            <td><a>" . $tablelinha['grimcardname'] . "</a></td>
                        </tr>";
                    }
                }
            }?>
            </tbody>
        </table>
    </div>
</div>

<?php pg_close($db);?>
</body>
</html>