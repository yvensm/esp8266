﻿drop view if exists listadecks;
CREATE OR REPLACE VIEW LISTADECKS AS
SELECT 
  deck.id, 
  deck.nome, 
  "DCT".nome AS "deckcardname", 
  deck_card.qtd AS "deckqtd"
FROM 
  public.deck, 
  public.deck_card, 
  public.cartas "DCT"
WHERE 
  deck_card.iddeck = deck.id AND
  deck_card.idcard = "DCT".id
ORDER BY
  deck.id ASC;

CREATE OR REPLACE VIEW LISTAGRIMORIO AS
SELECT 
  cartas.id AS idcard, 
  cartas.nome, 
  grimorio.qtd
FROM 
  public.grimorio, 
  public.cartas
WHERE 
  grimorio.idcard = cartas.id;


SELECT * FROM LISTADECKS;