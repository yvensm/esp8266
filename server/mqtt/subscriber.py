import sys
import paho.mqtt.client as mqtt
import psycopg2



broker = "ifce.sanusb.org"
port = 1883
keepAlive = 60
topic = 'esp/yvens/#'

def insertLOG(uid):

	# decript=''

	# for i in range(0,8,2):
	# 	decript=decript+str((hex(int(uid[i]+uid[i+1],16)^ 113)[2:]))

	# uid=decript
	# print uid
	
	con =psycopg2.connect(host='127.0.0.1',database='Controle', user='postgres', password='postgres')

	if not con:
		print 'erro ao conectar com o bd'

	cur = con.cursor()
	try:
		sql= """SELECT ACESSO(%s);"""
		print sql
		cur.execute(sql,(uid,))
		con.commit()
	except:
		print 'Nao deu pra salvar'
	con.close()

def insertUID(uid):


	# decript=''

	# for i in range(0,8,2):
	# 	decript=decript+str((hex(int(uid[i]+uid[i+1],16)^ 113)[2:]))

	# uid=decript


	con =psycopg2.connect(host='127.0.0.1',database='Controle', user='postgres', password='postgres')

	if not con:
		print 'erro ao conectar com o bd'

	cur = con.cursor()
	try:
		sql= """INSERT INTO usuario(uid) VALUES (%s);"""
		print sql
		cur.execute(sql,(uid,))
		con.commit()
	except:
		print 'Nao deu pra salvar'
	con.close()

def on_connect(client , userdata, flahgs, rc):
    print("[STATUS] Conectado ao Broker. Resultado de conexao" + str(rc))
    client.subscribe(topic)

def on_message(client, userdata, msg):
    message = str(msg.payload)
    print("[MSG] RECEBIDA Topico: " + msg.topic + "/ Menssagem: "+message)

    if msg.topic == 'esp/yvens/rfidins':
    	print 'inserindo novo UID'
    	insertUID(message)

    elif msg.topic == 'esp/yvens/rfidlog':
    	print 'inserindo log'
    	insertLOG(message)



try:
	print("[STATUS] Inicializando MQTT...")

	client = mqtt.Client()
	client.on_connect = on_connect
	client.on_message = on_message

	client.connect(broker,port,keepAlive)
	client.loop_forever()

except KeyboardInterrupt:
	print '\nScript finalizado'
	sys.exit(0)



