

<?php

    $host        = "host = localhost";//192.168.0.104";
    $port        = "port = 5432";
    $dbname      = "dbname = Controle";
    $credentials = "user = postgres password=postgres";


    $db = pg_connect( "$host $port $dbname $credentials");
    if(!$db) {
        die( "Error : Unable to open database");
    }

    $sql = "SELECT id,uid,nome,to_char(hora,'dd/mm/yy hh24:mi') as tempo FROM USUARIO WHERE NOME IS NULL";


    $con = pg_query($db,$sql) or die("Deu errado");



?>


<html>
    <head>
        <meta charset="UTF-8">
        <title>Cadastro de Nomes</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <script>
            function activeLine(nLine) {
                var linha = document.getElementById("linhaAtiva");
                if(linha){
                   linha.id= "linha"+linha.value;
                   linha.setAttribute("onclick","activeLine("+linha.value+")");
                   linha.setAttribute("class","");
                }
                linha = document.getElementById("linha"+nLine);
                linha.value = nLine;
                linha.classList.add("active-line");

                linha.id = "linhaAtiva";
                linha.setAttribute("onclick", "");

                uid = document.querySelector("#linhaAtiva td.uid");

                uid= uid.innerText;
                uidCamp = document.getElementById("uidSelected");
                uidCamp.value = uid;

            }

        </script>

        <style>
            .active-line{
                background: #1c449d !important;
                color: #fff;
            }
        </style>


    </head>

    <body>
        <div style="padding-top: 50px" class="container">
            <h1 style="text-align: center; margin-bottom: 50px;" center">Cadastro de Nomes</h1>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">UID</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Timestamp</th>
                    </tr>
                </thead>
                <tbody>
                <?php while($dado = pg_fetch_array($con)){?>
                <tr value="<?php echo $dado["id"];?>" id="linha<?php echo $dado["id"];?>" onclick="activeLine(<?php echo $dado["id"];?>)">
                    <th scope="row"><?php echo $dado["id"];?></th>
                    <td class="uid"><?php echo $dado["uid"];?></td>
                    <td><?php echo $dado["nome"];?></td>
                    <td><?php echo $dado["tempo"];?></td>
                </tr>

                <?php
                    }
                    pg_close($db);
                ?>
                </tbody>
            </table>
            <form method="get" action="update.php">
                <div class="form-group">
                    <label style="display: none;" for="uid">UID</label>
                    <input id="uidSelected" class="form-control" name="uid" type="hidden" autocomplete="off">
<!--                    <p name="uid" id="uidSelected"></p>-->

                </div>
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input name="name" type="nome" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Ex: Francisco" autocomplete="off">
                    <small id="emailHelp" class="form-text text-muted">Insira o nome do usuário selecionado.</small>
                </div>
                <!--<div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="checkConfirmAccess">
                    <label class="form-check-label" for="checkConfirmAccess">Conceder Acesso ao usuário selecionado.</label>
                </div>-->
                <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>

            </form>
            <a href="index.php" role="button" class="btn btn-primary btn-lg btn-block">Voltar</a>
        </div>
    </body>
</html>