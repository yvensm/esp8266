<?php
$data= $_POST['data'];


$host        = "host = 127.0.0.1";//192.168.0.104";
$port        = "port = 5432";
$dbname      = "dbname = Controle";
$credentials = "user = postgres password=postgres";
$dataserver='';

$db = pg_connect( "$host $port $dbname $credentials");
if(!$db) {
    die( "Error : Unable to open database");
}

if(!$data) {
    $socket = fsockopen('udp://pool.ntp.br', 123, $err_no, $err_str, 1);
    if ($socket) {
        if (fwrite($socket, chr(bindec('00' . sprintf('%03d', decbin(3)) . '011')) . str_repeat(chr(0x0), 39) . pack('N', time()) . pack("N", 0))) {
            stream_set_timeout($socket, 1);
            $unpack0 = unpack("N12", fread($socket, 48));
            $dataserver = date('d/m/y', $unpack0[7]);
        }
    }
    fclose($socket);
}else{
    $dataserver=$data;
}

$sql = "SELECT nome,to_char(hora,'dd/mm/yy hh24:mi') as tempo from logdeacesso where date(hora) = '$dataserver' order by hora desc";



$con = pg_query($db,$sql) or die("Deu errado");



?>


<html>
<head>
    <meta charset="UTF-8">
    <title>Controle de Acesso</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
    <script src="js/jquery.min.js" type="text/javascript" ></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/bootstrap-datepicker.min.js" type="text/javascript" ></script>
    <script src="locales/bootstrap-datepicker.pt-BR.min.js" type="text/javascript" ></script>

</head>
<body>
<div style="padding-top: 50px" class="container">
    <h1 style="text-align: center;margin-bottom: 50px;" center">Registros de Acesso</h1>
    <div class="pre-scrollable">
    <table  class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Nome</th>
            <th scope="col">Timestamp</th>

        </tr>
        </thead>
        <tbody>
        <?php while($dado = pg_fetch_array($con)){?>
            <tr>
                <td><?php echo $dado["nome"];?></td>
                <td><?php echo $dado['tempo'];?></td>

            </tr>

            <?php
        }
        pg_close($db);
        ?>
        </tbody>
    </table>
    </div>
    <form method="post" action="#">

        <div  style="border-color: #0044cc; border: 2px;" class="form-group">
            <label for="data">Selecione o Dia:</label>
            <input name="data" type="text" class="datepicker form-control-sm" autocomplete="off">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
            </div>
        </div>
        <input type="submit" class="btn btn-primary btn-lg btn-block" role="button">
    </form>
<!--    <a href="logacesso.php" role="button" class="btn btn-primary btn-lg btn-block">Refresh</a>-->

    <a href="index.php" role="button" class="btn btn-primary btn-lg btn-block">Voltar</a>
    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language: "pt-BR"
        });
    </script>
</div>


</body>
</html>