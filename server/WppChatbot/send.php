<?php
// Exibe todos os erros PHP
error_reporting(-1);
	
$data = [
    'phone' => '5585988850881', // Receivers phone
    'body' => 'Testando msg via aplicação', // Message
];

$json = json_encode($data); // Encode data to JSON
// URL for request POST /message
$url = 'https://eu25.chat-api.com/instance40840/message?token=bmufdbzqqwyzsns7';
// Make a POST request
$options = stream_context_create(['http' => [
        'method'  => 'POST',
        'header'  => 'Content-type: application/json',
        'content' => $json
    ]
]);
// Send a request
$result = file_get_contents($url, false, $options);
