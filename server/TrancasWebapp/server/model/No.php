<?php
/**
 * Created by PhpStorm.
 * User: yvens
 * Date: 14/11/18
 * Time: 07:03
 */

class No
{
    private $id;
    private $chipid;
    private $nome;
    private $hora;

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

        /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }/**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }/**
     * @return mixed
     */
    public function getChipid()
    {
        return $this->chipid;
    }/**
     * @param mixed $chipid
     */
    public function setChipid($chipid)
    {
        $this->chipid = $chipid;
    }/**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }/**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }


}