<?php
/**
 * Created by PhpStorm.
 * User: yvens
 * Date: 14/11/18
 * Time: 07:06
 */

class ConnectionFactory
{
    private $conn;


    public function getConnectionFactory(){
        $host        = "host = 127.0.0.1";
        $port        = "port = 5432";
        $dbname      = "dbname = Controle";
        $credentials = "user = postgres password=postgres";

        $this->conn = pg_connect("$host $port $dbname $credentials");

        return $this->conn;
    }

    public function queryExec($sql){
        if(!pg_query($conn,$sql)){
            return false;
        }
        return true;
    }

    public function close(){
        pg_close($this->conn);
    }

}
