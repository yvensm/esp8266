<!DOCKTYPE html>
<html>
<head>

    <script src="_js/jquery.js"></script>
    <link rel="stylesheet" href="_css/bootstrap.min.css">
    <script src="_js/bootstrap.min.js"></script>
<!--    <link rel="stylesheet" href="_css/material-icons.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="_css/header.css">

    <title>Home</title>
</head>
<body>
<div id="my-header">
    <?php
    include ("navbar.php");
    ?>
</div>
<div class="container">


    <div id="my-content">
        <h1>Home Sweet Home</h1>
    </div>
    <div id="my-footer"></div>
</div>

</body>
</html>