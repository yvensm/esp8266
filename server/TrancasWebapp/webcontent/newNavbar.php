
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">

        <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_target">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home.php">Home</a>
            </li>
            <li>
                <a class="nav-link" href="cadastro.php">Cadastro de Usuarios</a>
            </li>
            <li>
                <a class="nav-link"" href="gerenciarAmbientes.php">Gerenciar Ambientes</a>
            </li>
        </ul>
        </div>
    </nav>
