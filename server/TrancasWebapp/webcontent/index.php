<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
    <link rel="stylesheet" href="_css/style.css">
    <link type="text/css" rel="stylesheet" href="_css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="_css/bootstrap-grid.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="_js/bootstrap.min.js"></script>

</head>
<body class="text-center">

    <form action="../server/View/Login.php" class="form-signin" method="post">
        <img class="mb-4" src="_images/icon.png" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">DoorLockSys</h1>
        <label for="usuario" class="sr-only">Usuario</label>
        <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuário" required="" autofocus="">
        <label for="senha" class="sr-only">Password</label>
        <input type="password" name="senha" id="senha" class="form-control" placeholder="Senha" required="">

        <?php
        if(!empty($_GET['ee'])){
            echo "<small id=\"senhaIncorreta\" style=\"color: #ff3831;\" class=\"form-text\">Senha ou usuário incorreto.</small>";
        }

        ?>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">Copyright © 2018 Yvens Martins</p>
    </form>


</body>

</html>