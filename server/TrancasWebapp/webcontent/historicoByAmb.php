<!DOCKTYPE html>
<html>
<head>

    <script src="_js/jquery.js"></script>
    <link rel="stylesheet" href="_css/bootstrap.min.css">
    <script src="_js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="_css/header.css">
    <title>Histórico</title>
</head>
<body>
<div id="my-header">
    <?php
    include ("navbar.php");

    $id = $_POST['id'];
    ?>
</div>
<div class="container">


    <div id="my-content">
        <div class="container">
            <div class="pre-scrollable">
                <table id="amb-table" class="table table-sm table-hover">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Data</th>
                    </tr>

                    <tbody>
                    <tr>
                        <?php
                        ini_set('display_errors',1);
                        ini_set('display_startup_erros',1);
                        set_include_path("{$_SERVER['DOCUMENT_ROOT']}/TrancasWebapp");
                        include_once('server/View/logEntradaView.php');

                        $logView = new logEntradaView();
                        $logView->getLogTableById($id);
                        ?>
                    </tr>
                    </tbody>
                    </thead>

                </table>

            </div>
        </div>
    </div>
    <div id="my-footer"></div>
</div>

</body>
</html>