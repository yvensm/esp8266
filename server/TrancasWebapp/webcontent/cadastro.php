<!DOCKTYPE html>
<html>
<head>
    <script src="_js/jquery.js"></script>
    <link rel="stylesheet" href="_css/bootstrap.min.css">
    <script src="_js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="_css/header.css">
    <title>Cadastro de Usuários</title>
</head>
<body>
<div id="my-header">
    <?php
    include("navbar.php");
    ?>
</div>

    <div id="my-content" class="container">
        <form action="../server/Controller/InsertUsuarioController.php" method="post">
            <div class="form-group">
                <label for="nome">Nome: </label>
                <input name="nome" type="text" class="form-control" id="nome">
            </div>
            <div class="form-group">
                <label for="uid">UID: </label>
                <input name="uid" style="margin-bottom: 10px" type="text" class="form-control" id="uid">

                    <?php
                     ini_set('display_errors',1);
                        ini_set('display_startup_erros',1);
                        include_once ('../server/View/NosView.php');
                        $nosView = new NosView();
                        $nosView->getSelectList();
                    ?>
                <br>
            <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
            </div>
        </form>
    </div>

<div id="my-footer"></div>
</body>
</html>