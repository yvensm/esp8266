<!DOCKTYPE html>
<html>
<head>

    <script src="_js/jquery.js"></script>
    <link rel="stylesheet" href="_css/bootstrap.min.css">
    <script src="_js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="_css/header.css">
    <title>Gerenciar Ambientes</title>

</head>
<body>
<div id="my-header">
    <?php
    include("navbar.php");
    ?>
</div>

<div id="my-content" class="container">
    <div class="pre-scrollable">
        <table id="amb-table" class="table table-sm table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Ambiente</th>
            </tr>

            <tbody>
            <tr>
                <?php
                    set_include_path("{$_SERVER['DOCUMENT_ROOT']}/TrancasWebapp");
                    include_once('server/View/NosView.php');

                    $noview = new NosView();
                    $noview->getNosTable();
                ?>
            </tr>
            </tbody>
            </thead>

        </table>

    </div>
    <button type="button" class="btn btn-primary btn-lg btn-block">Editar</button>
    <button type="button" id="historicoBtn" class="btn btn-primary btn-lg btn-block">Historico</button>

</div>

<div id="my-footer"></div>

<script>
    $('#historicoBtn').click(function () {


        var $lin = $('.selected .collum-1');
        if(!$lin[0]){
            alert('Selecione um ambiente!');
            return;
        }
        $lin=$lin.text();
        // $.redirect("historicoByAmb.php", {id: $lin});
        var url = 'historicoByAmb.php';
        var form = $('<form style="display:none" action="' + url + '" method="post">' +
            '<input type="text" name="id" value='+$lin+' />' +
            '</form>');
        $('body').append(form);
        $(form).submit();
    });

    $('#amb-table tr').click( function(){
        if($('.selected')[0] == $(this)[0]){
            $aux= $('.selected');
            $aux.removeClass('selected');
            $aux.css('background-color',"white");
            $aux.css('color','black');
            return;
        }else{
            if($('.selected')[0]){
                $aux= $('.selected');
                $aux.removeClass('selected');
                $aux.css('background-color',"white");
                $aux.css('color','black');
            }
            $(this).addClass("selected");
            $(this).css('background-color', "355196");
            $(this).css('color','white');

        }




    });
</script>
</body>
</html>