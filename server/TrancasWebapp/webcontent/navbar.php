


<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">

    <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="collapse_target">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home.php"><i class="fa fa-fw fa-home"></i>Home</a>
            </li>
            <li>

                <a class="nav-link" href="cadastro.php"><i class="fa fa-fw fa-user"></i>Cadastro de Usuarios</a>
            </li>
            <li>
                <a class="nav-link" href="gerenciarAmbientes.php"><i class="fa fa-fw fa-cogs"></i>Gerenciar Ambientes</a>
            </li>
        </ul>
    </div>
</nav>
