package br.com.yvens.drogaria.main;

import org.hibernate.Hibernate;
import org.hibernate.Session;

import br.com.yvens.drogaria.util.HibernateUtil;

public class HibernateUtilTeste {
	public static void main() {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.close();
		HibernateUtil.getSessionFactory().close();
	}

}
