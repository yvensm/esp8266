#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "RTClib.h"
#include <stdio.h>
#include <string.h>
#include <FS.h>

#define TV D3
#define LUZESEXT D4
#define LUZESINT D5
#define CONTROLMODE D6
#define ON LOW
#define OFF HIGH

RTC_DS1307 rtc;
const char* ssid= "MNET_YSLA";
const char* password = "ysla657283";
char daysOfTheWeek[7][12] = {"Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"};

char tvTimes[7][6]={"1:00","1:00","1:00","1:00","1:00","1:00","1:00"};
char luzExtTimes[7][6]={"2:00","2:00","2:00","2:00","2:00","2:00","2:00"};
char luzIntTimes[7][6]={"3:00","3:00","3:00","3:00","3:00","3:00","3:00"};

char controlModeAutoOff[6]="7:00";
char controlModeAutoOn[6]="16:00";

const char colors[2][6]={"red","green"};

// const char *mqtt_server = "192.168.0.105";
// const int mqtt_port = 1884;
// const char *topic = "esp/restaurante";


// WiFiClient espClient;
// PubSubClient client(espClient);

WiFiServer server(81);
String header;

int disp[4];
char states[4][6];

//PROTOTYPES
void callback(char *topic, byte *payload, unsigned int length);
void initSerial();
void initWiFi();
void initMQTT();
void reconnectMQTT();
void recconectWiFi();
void initPins();
void initRTC();
void initSPIFFS();
void jsonRequest();
String getPage();
void clientServer();
void initFiles();
void initDevices();
void changeStates();

//MAIN
void setup(){
  initSerial();
  initWiFi();
  //initMQTT();
  initRTC();
  initPins();
  server.begin();
  SPIFFS.begin();
  //SPIFFS.format();
  initFiles();
  initDevices();
}

void loop(){

  // if(!client.connected()){
  //   reconnectMQTT();
  // }
  recconectWiFi();
  clientServer();
  //client.loop();
  DateTime now = rtc.now();
  Serial.print("Data: ");
  Serial.print(now.day());
  Serial.print("Dia da Semana: ");
  Serial.print(now.dayOfTheWeek());
  Serial.print("Hora: ");
  Serial.print(now.hour());
  Serial.print(":");
  Serial.print(now.minute());
  Serial.print(":");
  Serial.print(now.second());
  Serial.println();
  for(int i=0;i<4;i++){
    Serial.print(disp[i]);
    Serial.print(" @ ");
   // Serial.println(states[i]);
  }
  Serial.println();
  delay(200);
}
//MAIN

void decideAction(char *device, char *action){
  
}


void callback(char *topic, byte *payload, unsigned int length){
  //command pattern "device"@"action"
  //ex TV@ON
  char mensage[length];
  for (int i = 0; i < length; i++){
    mensage[i]= (char)payload[i];
  }
  char *ptr = strtok(mensage,"@");
  
  char *device;
  char *action;

  device=ptr;
  ptr = strtok(NULL, "@");
  action=ptr;
  
  if(!strcmp(action,"ON")){
    Serial.println("LIGA");
  }
 // mensageGetAction(&mensage,&device,&action);

  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  Serial.println(device);
  Serial.println(action);

  Serial.println("-----------------------");

}


void initPins(){
  pinMode(TV,OUTPUT);
  pinMode(LUZESEXT,OUTPUT);
  pinMode(LUZESINT,OUTPUT);
  pinMode(CONTROLMODE,OUTPUT);
  
  digitalWrite(TV,OFF);
  digitalWrite(LUZESEXT,OFF);
  digitalWrite(LUZESINT,OFF);
  digitalWrite(CONTROLMODE,OFF);
}

void initSerial(){
  Serial.begin(9600);
}

void initWiFi(){
  delay(10);
  Serial.println("Conectando-se em: " + String(ssid));

  WiFi.begin(ssid,password);

  while (WiFi.status() != WL_CONNECTED){
    delay(100);
    Serial.print(".");
  }
  Serial.print("Conectado na Rede " + String(ssid) + " | IP => ");
  Serial.println(WiFi.localIP());
}

// void initMQTT(){
//   client.setServer(mqtt_server,mqtt_port);
//   client.setCallback(callback);
// }

// void reconnectMQTT() {
//   while (!client.connected()) {
//     Serial.println("Tentando se conectar ao Broker MQTT: " + String(mqtt_server));
//     if (client.connect("ESP8266-ESP12-E")) {
//       Serial.println("Conectado");
//       client.subscribe(topic);

//     } else {
//       Serial.println("Falha ao Reconectar");
//       Serial.println("Tentando se reconectar em 2 segundos");
//       delay(2000);
//     }
//   }
// }

void recconectWiFi() {
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }
}

void initSPIFFS(){
  SPIFFS.begin();
}

void initRTC(){
  Wire.begin(5,4);
  if(rtc.begin()){
    //rtc.adjust(DateTime(__DATE__,__TIME__));
    Serial.println("RTC Rodando.");
  }
}


String getPage(){
  String html ="<html> <header> <title>STL(Shutdown Time Limit)</title> <meta charset=\"UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\"> <style type=\"text/css\"> .container{ margin: 0 auto; width: 95%; } button{ margin: 10px; } html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;} button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;} </style> </header> <body> <div class=\"container\"> <h1>Sistema de tempo limite de funcionamento.</h1> <br> <h1>TV</h><br>";
  
  if(!disp[0]){
    html+= "<a href=\"/1/on\"><button style=\"background-color: red;\">ON</button></a>";
  }else {
    html+= "<a href=\"/1/off\"><button style=\"background-color: green;\">OFF</button></a>";
  }
  html+="<br> <h1>Luzes Externas</h><br>";
  if(!disp[1]){
    html+= "<a href=\"/2/on\"><button style=\"background-color: red;\">ON</button></a>";
  }else {
    html+= "<a href=\"/2/off\"><button style=\"background-color: green;\">OFF</button></a>";
  }
  html+="<br> <h1>Luzes Internas</h><br>";

  if(!disp[2]){
    html+= "<a href=\"/3/on\"><button style=\"background-color: red;\">ON</button></a>";
  }else {
    html+= "<a href=\"/3/off\"><button style=\"background-color: green;\">OFF</button></a>";
  }
  html+="<br> <h1>Modo Controle</h><br>";
  if(!disp[3]){
    html+= "<a href=\"/4/on\"><button style=\"background-color: red;\">ON</button></a>";
  }else {
    html+= "<a href=\"/4/off\"><button style=\"background-color: green;\">OFF</button></a>";
  }
  html+="</div> </body> </html>";

  return html;
}


void clientServer(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /1/on") >= 0) {
              Serial.println("TV on");
              disp[0]=1;
              strcpy(states[0],"green");
              //digitalWrite(TV, ON);
            } else if (header.indexOf("GET /1/off") >= 0) {
              Serial.println("TV off");
              disp[0]=0;
              strcpy(states[0],"red");
              //digitalWrite(TV, OFF);
            } else if (header.indexOf("GET /2/on") >= 0) {
              Serial.println("LUZES EXTERNAS on");
              disp[1]=1;
              strcpy(states[1],"green");
              //digitalWrite(LUZESEXT, ON);
            } else if (header.indexOf("GET /2/off") >= 0) {
              Serial.println("LUZES EXTERNAS off");
              disp[1]=0;
              strcpy(states[1],"red");
              //digitalWrite(LUZESEXT, OFF);
            } else if (header.indexOf("GET /3/on") >= 0) {
              Serial.println("LUZES INTERNAS on");
              disp[2]=1;
              strcpy(states[2],"green");
              //digitalWrite(LUZESINT, ON);
            } else if (header.indexOf("GET /3/off") >= 0) {
              Serial.println("LUZES INTERNAS off");
              disp[2]=0;
              strcpy(states[2],"red");
              //digitalWrite(LUZESINT, OFF);
            }else if (header.indexOf("GET /4/on") >= 0) {
              Serial.println("MODO CONTROLE on");
              disp[3]=1;
              strcpy(states[3],"green");
              //digitalWrite(CONTROLMODE, ON);
            }else if (header.indexOf("GET /4/off") >= 0) {
              Serial.println("MODO CONTROLE off");
              disp[3]=0;
              strcpy(states[3],"red");
              //digitalWrite(CONTROLMODE, OFF);
            }else{
              Serial.println("Tão tentando registar especial");
            }
            changeStates();
            client.println(getPage());//send the html page to client
            client.println();

            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}


void initFiles(){
  if(!SPIFFS.exists("devices.txt")){
    Serial.println("Arquivo de dispositivos não encontrado, inicializando...");
    File f = SPIFFS.open("devices.txt","w");
    f.println("0.0.0.0.");
    f.close();
  }

}

void initDevices(){
  File f;
  f=SPIFFS.open("devices.txt","r");
  char read[9];
  String line;
  line = f.readStringUntil('\n');
  f.close();
  line.toCharArray(read, 9);
  Serial.println(line);
  char *ptr = strtok(read,".");
  
  for(int i=0;i<4;i++){
   
    disp[i]= (int) strtol(ptr, (char **)NULL, 10);
    Serial.print("Device ");
    Serial.print(i);
    Serial.print("= ");
    Serial.print(disp[i]);
    ptr = strtok(NULL, ".");
  }

  changeStates();
 

}

void changeStates(){
  if(disp[0]){
    digitalWrite(TV,ON);
  }else{
    digitalWrite(TV,OFF);
  }
  if(disp[1]){
    digitalWrite(LUZESEXT,ON);
  }else{
    digitalWrite(LUZESEXT,OFF);
  }
  if(disp[2]){
    digitalWrite(LUZESINT,ON);
  }else{
    digitalWrite(LUZESINT,OFF);
  }
  if(disp[3]){
    digitalWrite(CONTROLMODE,ON);
  }else{
    digitalWrite(CONTROLMODE,OFF);
  }


  File f = SPIFFS.open("devices.txt","w");
  for(int i = 0; i<4;i++){
    f.print(disp[i]);
    f.print('.');
  }
  f.println();
  f.close();
}