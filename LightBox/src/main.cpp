#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>


const char* ssid= "Yvens";
const char* password = "asd123456";

ESP8266WebServer server(80); 

int porcentagem = 50;

void getPage();

void changePWM(){
  if(server.arg("porc")==""){
    Serial.println("Valor invalido");
  }else{
    Serial.println(server.arg("porc"));
  }
  Serial.println("Deu certo");
  getPage();
}
void setup() {
  Serial.begin(9600);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid,password);
  Serial.print("Conecting");
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected");
  server.on("/args",changePWM);
  server.on("/",getPage);
  server.begin();
  //WiFi.softAP(ssid,password);
  Serial.println(WiFi.localIP());
  pinMode(D0, OUTPUT);
  // Serial.print("Access Point IP :");
  // Serial.print(WiFi.softAPIP());
  // Serial.println(WiFi.softAPSSID());

}

void loop() {
  //WiFiClient client = server.available();
  
  //server.handleClient();
  digitalWrite(D0,HIGH);
  delay((1000*porcentagem)/100);
  digitalWrite(D0,LOW);
  delay((1000*(100-porcentagem))/100);
  // if(!client) return;

  // Serial.println("New Client!");
  // while(!client.available()){
  //   delay(1);
  // }

  // //String peticion = client.readStringUntil('\r');
  // String peticion = client.readStringUntil('\n');
  // Serial.println(peticion);
  // client.flush();

  // Serial.print(peticion);

  // client.println("HTTP/1.1 200 OK");
  // client.println("");              
  // client.println(getPage());
  // client.println();
}

void getPage(){
  String html ="<html> <header> <title>LightBox</title> <meta charset=\"UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\"> <style type=\"text/css\"> .container{ margin: 0 auto; width: 95%; display: inline-block; text-align: center; } </style> <script type=\"text/javascript\"> function changePorc(){ var input = document.getElementById(\"porcInput\"); console.log(input.value); var url=\"/args?porc=\"; url+=input.value; window.location.href=url; } </script>> </header> <body> <div class=\"container\"> <h1 style=\"color:blue;\">Light Box Control</h1> <br> <label>Porcentagem de brilho:</label> <input type=\"text\" name=\"porc\" id=\"porcInput\"> <br><br> <button style=\"width: 100px;height: 60px\" onclick=\"changePorc()\">Enviar</button> </div> </body> </html>";

  server.send(200,"text/html",html);
}