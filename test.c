#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(){

	FILE *arq;
	char read[50];
	uint8_t id[4]={0xE3,0xE3,0xE3,0xE3};
	uint8_t readid[4];
	
	arq= fopen("data","a");

	for(int i = 0;i < 4;i++){
		fprintf(arq,"%x",id[i]);
	}
	fprintf(arq, "\n");

	fclose(arq);
		
	arq=fopen("data","r");

	fgets(read,50,arq);
	
	while(!feof(arq)){
		printf("%s\n", read);
		
		uint32_t number = strtoul(read,NULL,16);
		for (int i = 3; i >=0 ; i--){
			readid[i]= number & 0xFF;
			number >>=8;
		}

		if (id[0]!=readid[0] |
		id[1]!=readid[1] |
		id[2]!=readid[2] |
		id[3]!=readid[3]){
			printf("%d %d\n", readid[0],id[0]);
			printf("%d %d\n", readid[1],id[1]);
			printf("%d %d\n", readid[2],id[2]);
			printf("%d %d\n", readid[3],id[3]);
			printf("Acesso Negado!!\n");
			fgets(read,50,arq);
		}else{
			printf("Acesso Concedido!!!\n");
			break;
		}
	}
	
	/*printf("%s\n",read);

	uint32_t number = strtoul(read,NULL,16);
	for (int i = 3; i >=0 ; i--){
		readid[i]= number & 0xFF;
		number >>=8;
	}

	if (id[0]!=readid[0] |
		id[1]!=readid[1] |
		id[2]!=readid[2] |
		id[3]!=readid[3])
	{
		printf("True\n");
	}
	fclose(arq);*/
	return 1;
}
