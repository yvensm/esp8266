#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>


const char* ssid= "MNET_YSLA";
const char* password = "ysla657283";

void setup(){
  Serial.begin(9600);
  WiFi.begin(ssid,password);

  Serial.print("Conecting");
  if(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected");


}

void loop(){

  Serial.println("Trying to connect to the server.");
  uint32_t id = system_get_chip_id();
  Serial.print("Chip id:");
  Serial.println(id);
  if(WiFi.status() == WL_CONNECTED){
      int tam = 0;
      HTTPClient http;
      http.begin("http://192.168.0.106/TrancasWebapp/server/Controller/jsonRequest.php?op=0");
      int httpCode = http.GET();
      
      if(httpCode > 0){
        Serial.println("Server Mensage Receved.");
        const size_t bufferSize = JSON_OBJECT_SIZE(1) + 30;
        DynamicJsonBuffer jsonBuffer(bufferSize);
        JsonObject& root = jsonBuffer.parseObject(http.getString());

        tam = root["tam"];
        // Output to serial monitor
        delay(1000);
        http.end();
      }
      http.begin("http://192.168.0.106/TrancasWebapp/server/Controller/jsonRequest.php?op=1");
      int httpCode = http.GET();

      if(httpCode > 0){
        Serial.println();

        const size_t bufferSize = JSON_ARRAY_SIZE(tam) + JSON_OBJECT_SIZE(1) + (tam+1)*10;
        DynamicJsonBuffer jsonBuffer(bufferSize);
        JsonObject& root = jsonBuffer.parseObject(http.getString());
        Serial.println("IDs Recebidos: ");
        for(int i=0;i<tam;i++){
          const char* str = root["uid"][i];
          Serial.println(str);
        }
      }
  }
  delay(1000);
}