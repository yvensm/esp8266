﻿CREATE VIEW LOGDEENT AS 
SELECT 
  no_usu.idno AS id_no, 
  nos.nome AS nome_no, 
  usuario.nome AS nome_usu, 
  no_usu.idusu AS id_usu, 
  log_ent.hora
FROM 
  public.log_ent, 
  public.no_usu, 
  public.nos, 
  public.usuario
WHERE 
  log_ent.idno = nos.id AND
  no_usu.idusu = log_ent.idusu AND
  no_usu.idno = log_ent.idno AND
  usuario.id = no_usu.idusu;
