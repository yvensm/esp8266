#include <SPI.h>
#include <MFRC522.h>
#include <FS.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#define SS_PIN 15     //pin d8
#define RST_PIN 4     //pin d2
#define buzzer 10     //pin sdd3
#define ledVerde 16   //pin d0
#define ledVermelho 5 //pin d1
#define porta 2       //pin d4
#define botao 0       //unused

const char *ssid = "ifce-alunos";
const char *password = "ifce4lun0s";

WiFiServer server(80);

const char *mqttServer = "ifce.sanusb.org";
const int mqttPort = 1883;

const char *uidInsertTopic = "esp/yvens/rfidins";
const char *logTopic = "esp/yvens/rfidlog";

//Servidor HTTP
const char *http_server = "10.50.25.191";
const int http_port = 80;

IPAddress server_http(192, 168, 43, 188);

WiFiClient espClient;
PubSubClient client(espClient);

//Instancia da classe
MFRC522 reader(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key;

bool match = false;        //ID encontrado no storage
bool programMode = false;  //Modo programador
bool repaceMaster = false; //Substituir master
bool modoOFF = false; //Modo Offline

uint8_t sucessRead; //Informa se houve sucesso na leitura

byte readID[4];   //ID Lido pelo leitor
byte storeID[4];  //ID Lido do storage
byte masterID[4]; //ID do master

byte xorKey = 133;

uint8_t getID();
boolean checkTwo(byte a[], byte b[]);
boolean isMaster(byte master[]);
boolean findID(byte find[]);
boolean removeID(byte a[]);

void writeID(byte a[]);
void concederAcesso();
void acessoNegado();
bool serverAction(byte uid[], uint8_t op);

void acessoNegado()
{
  digitalWrite(ledVermelho, LOW);
  delay(700);
  digitalWrite(ledVermelho, HIGH);
}

void concederAcesso()
{

  digitalWrite(ledVerde, LOW);
  delay(50);
  digitalWrite(porta, LOW);
  delay(10);
  digitalWrite(porta, HIGH);
  serverAction(readID, 1);
  delay(700);
  digitalWrite(ledVerde, HIGH);
  byte bufferATQA[10];
  byte bufferSize[10];
  reader.PICC_WakeupA(bufferATQA, bufferSize);
  delay(100);
  reader.PICC_RequestA(bufferATQA, bufferSize);
  delay(100);
}

void initWiFi(){
  delay(10);
  Serial.println("Conectando-se em: " + String(ssid));
  
  WiFi.begin(ssid,password);
  for(int i=0;i<1000;i++){
    if(WiFi.status() != WL_CONNECTED){
      delay(100);
      Serial.print(".");
    }
    else{
      Serial.print("Conectado na Rede " + String(ssid) + " | IP => ");
      Serial.println(WiFi.localIP());
      return;
    }
  }
  modoOFF=true;
  
}

void masterInit(){
  File master;
  if (!SPIFFS.exists("master.txt")){
    Serial.println("Master ainda não existe, entrando no modo de config");

    //master nao existe pisca vermelho 3x e deixa aceso
    for (int i = 0; i < 3; i++){
      digitalWrite(ledVermelho, LOW);
      delay(200);
      digitalWrite(ledVermelho, HIGH);
      delay(200);
    }
    digitalWrite(ledVermelho, LOW);

    do{
      sucessRead = getID();
      delay(200);
    } while (!sucessRead);
    master = SPIFFS.open("master.txt", "w");
    for (uint8_t i = 0; i < 4; i++)
    {
      master.print(readID[i] < 0x10 ? "0" : "");
      master.print(readID[i], HEX);
    }
    master.println("");
    master.close();
    //Ao o master desliga o led vermelho
    digitalWrite(ledVermelho, HIGH);
  }

  String line;
  Serial.println("Master encontrado!");
  master = SPIFFS.open("master.txt", "r");
  line = master.readStringUntil('\n');
  char cline[10];
  line.toCharArray(cline, 10);

  master.close();
  uint32_t number = strtoul(cline, NULL, 16);
  for (uint8_t j = 3; j < 255; j--)
  {
    masterID[j] = number & 0xFF;
    number >>= 8;
  }
  Serial.println("O master é ");
  for (uint8_t i = 0; i < 4; i++)
  {
    Serial.print(masterID[i] < 0x10 ? "0" : "");
    Serial.print(masterID[i], HEX);
  }
  Serial.println("");
}
void rfidRotine(){
  do
  {
    if (programMode)
    {
      digitalWrite(ledVerde, LOW);
      digitalWrite(ledVermelho, HIGH);
    }

    sucessRead = getID();
    delay(200);

    if (programMode)
    {
      digitalWrite(ledVerde, HIGH);
      digitalWrite(ledVermelho, LOW);
    }
  } while (!sucessRead);

  if (programMode)
  {
    if (isMaster(readID))
    { //Se ler o master, sair do modo programador.
      Serial.println(F("Master Lido"));
      Serial.println(F("Saindo do modo programador"));
      digitalWrite(ledVerde, HIGH);
      digitalWrite(ledVermelho, HIGH);
      programMode = false;
      return;
    }
    else
    {
      if (findID(readID))
      {
        //delete logic
        removeID(readID);
      }
      else
      {
        Serial.println(F("Novo id encontrado"));
        Serial.println(F("Adicionando permissão"));
        writeID(readID);
        Serial.println(F("Adicionado com sucesso!"));
      }
    }
  }
  else
  {
    if (isMaster(readID))
    { //Se for o master entrar no modo programador
      programMode = true;
      Serial.println(F("Ola Mestre - Entrando no modo programador"));
      Serial.println(F("-----------------------------------------"));
    }
    else
    {
      if (findID(readID))
      {

        client.publish("esp/yvensRfid", "Acesso Concedido, bem vindo ");
        Serial.println(F("Id cadastrado, acesso concedido"));
        concederAcesso();
      }
      else
      {
        client.publish("esp/yvensRfid", "Alerta! Usuario não cadastrado!");
        Serial.println(F("ID não cadastrado, acesso recusado"));
        acessoNegado();
      }
      Serial.println(F("--------------------------------------"));
    }
  }
}

void initPins(){
  pinMode(ledVerde, OUTPUT);
  pinMode(ledVermelho, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(botao, INPUT);
  pinMode(porta, OUTPUT);

  digitalWrite(ledVermelho, HIGH);
  digitalWrite(ledVerde, HIGH);
  digitalWrite(buzzer, LOW);
  digitalWrite(porta, HIGH);

}
void setup()
{
  Serial.begin(9600);
  SPI.begin();
  reader.PCD_Init();
  SPIFFS.begin();
  //SPIFFS.format();
  initWiFi();
  initPins();
  if (digitalRead(botao) == LOW){
    Serial.println("Tem certeza que deseja formatar?");
    delay(2000);
    if (digitalRead(botao) == LOW)
      SPIFFS.format();
  }

  masterInit();

  for (byte i = 0; i < 6; i++){
    key.keyByte[i] = 0xFF;
  }
  
  Serial.println(F("Setup Done"));
}


void loop()
{
  rfidRotine();
  delay(200);
}

boolean removeID(byte a[])
{
  File ids;
  File newarq;
  ids = SPIFFS.open("data.txt", "r");
  newarq = SPIFFS.open("new.txt", "w");

  while (ids.available())
  {
    String line;
    line = ids.readStringUntil('\n');
    char cline[10];
    line.toCharArray(cline, 10);
    uint32_t number = strtoul(cline, NULL, 16);
    for (uint8_t i = 3; i < 255; i--)
    {
      storeID[i] = number & 0xFF;
      number >>= 8;
    }

    if (!checkTwo(readID, storeID))
    {
      for (uint8_t i = 0; i < 4; i++)
      {
        newarq.print(readID[i] < 0x10 ? "0" : "");
        newarq.print(readID[i], HEX);
      }
      newarq.println("");
    }
  }
  ids.close();
  newarq.close();
  SPIFFS.remove("data.txt");
  SPIFFS.rename("new.txt", "data.txt");

  //Pisca 3x vermelho
  for (int i = 0; i < 3; i++)
  {
    digitalWrite(ledVermelho, LOW);
    delay(200);
    digitalWrite(ledVermelho, HIGH);
    delay(200);
  }

  return true;
}

boolean checkTwo(byte a[], byte b[])
{
  if (a[0] != 0)
    match = true;

  for (uint8_t i = 0; i < 4; i++)
  {
    if (a[i] != b[i])
      match = false;
  }
  if (match)
    return true;
  else
    return false;
}

boolean isMaster(byte test[])
{
  if (checkTwo(test, masterID))
  {
    return true;
  }
  else
    return false;
}

boolean findID(byte find[])
{
  File ids;
  ids = SPIFFS.open("data.txt", "r");

  while (ids.available())
  {
    String line;
    line = ids.readStringUntil('\n');
    char cline[10];
    line.toCharArray(cline, 10);
    uint32_t number = strtoul(cline, NULL, 16);
    for (uint8_t i = 3; i < 255; i--)
    {
      storeID[i] = number & 0xFF;
      number >>= 8;
    }
    Serial.println("Novo id lido do arq");

    if (checkTwo(find, storeID))
    {
      Serial.print(F("Storage: "));
      for (uint8_t i = 0; i < 4; i++)
      { //
        Serial.print(storeID[i] < 0x10 ? "0" : "");
        Serial.print(storeID[i], HEX);
      }
      Serial.println("\n");
      Serial.print(F("readID: "));
      for (uint8_t i = 0; i < 4; i++)
      {
        Serial.print(readID[i] < 0x10 ? "0" : "");
        Serial.print(readID[i], HEX);
      }
      Serial.println("\n");
      //Serial.println(F("ID encontrado, liberando acesso."));
      ids.close();
      return true;
      break;
    }
    Serial.print(F("Storage: "));
    for (uint8_t i = 0; i < 4; i++)
    { //
      Serial.print(storeID[i] < 0x10 ? "0" : "");
      Serial.print(storeID[i], HEX);
    }
    Serial.println("\n");
    Serial.print(F("readID: "));
    for (uint8_t i = 0; i < 4; i++)
    { //
      Serial.print(readID[i] < 0x10 ? "0" : "");
      Serial.print(readID[i], HEX);
    }
    Serial.println("\n");
  }
  ids.close();
  return false;
  
}

void writeID(byte a[])
{
  File data;

  data = SPIFFS.open("data.txt", "a");

  if (!findID(a))
  {
    for (uint8_t i = 0; i < 4; i++)
    {
      data.print(readID[i] < 0x10 ? "0" : "");
      data.print(readID[i], HEX);
    }

    data.println("");
    //Pisca 3x verde
    for (int i = 0; i < 3; i++)
    {
      digitalWrite(ledVerde, LOW);
      delay(200);
      digitalWrite(ledVerde, HIGH);
    }
    Serial.println("Novo id gravado com sucesso!");
    serverAction(readID, 0);
  }

  else
  {
    //Algum padrão de led
    Serial.println("Gravação falhou, algo errado não está certo!");
  }

  data.close();
}

uint8_t getID()
{
  //Procurando novo RFID
  if (!reader.PICC_IsNewCardPresent())
    return 0;
  //Verificando se o NUID foi lido
  if (!reader.PICC_ReadCardSerial())
    return 0;
  Serial.println(F("Scanned PICC's UID:"));
  for (uint8_t i = 0; i < 4; i++)
  { //
    readID[i] = reader.uid.uidByte[i];
    Serial.print(readID[i] < 0x10 ? "0" : "");
    Serial.print(readID[i], HEX);
  }
  Serial.println("");
  //buzinar
  for (int i = 0; i < 50; i++)
  {
    digitalWrite(buzzer, HIGH);
    delay(1);
    digitalWrite(buzzer, LOW);
  }
  reader.PICC_HaltA(); // Stop reading
  return 1;
}


bool serverAction(byte uid[], uint8_t op) // 0 insere novo usuario, 1 insere log
{

  //Criptografia básica do uid com Xor
  byte idcript[4];
  for (uint8_t i = 0; i < 4; i++)
  {
    idcript[i] = uid[i] ^ xorKey;
  }

  if (!espClient.connect(http_server, http_port))
  {
    Serial.println("Falha na conexao com o site ");
    return false;
  }

  String param = "?uid="; //Parâmetros com as leituras
  for (uint8_t i = 0; i < 4; i++)
  {
    param += idcript[i] < 0x10 ? "0" : "";
    param += String(idcript[i], HEX);
  }
  Serial.println(param);
  if (op == 0)
  {
    espClient.print("GET /esp/insert.php" + param + "\r\n" +
                    "Host: " + http_server + "Connection: close\r\n\r\n");
  }
  else if (op == 1)
  {
    espClient.print("GET /esp/log.php" + param + "\r\n" +
                    "Host: " + http_server + "Connection: close\r\n\r\n");
  }
  else if (op == 2)
  {
    //Remover usuario
  }
  /*espClient.println("Host: ");
    espClient.println(http_server);
    espClient.println("Connection: close\r\n\r\n");
    espClient.println();
    espClient.println(); */

  // Informações de retorno do servidor para debug
  /* while(espClient.available()){
    String line = espClient.readStringUntil('\r');
    Serial.print(line);
  } */
  return true;
}
