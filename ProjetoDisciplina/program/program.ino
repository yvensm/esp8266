#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#define RESET_PIN D1
#define LED D2

const char *ssid="Esp8266";
const char *password="asd123";
ESP8266WebServer server(80);

void handleRoot(){
  server.send(200,"text/html","<h1>You are connected</h1>");
}

void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.print();
  Serial.print("Configuring access point...");
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP adrress: " + myIP);
  server.on("/",handleRoot);
  server.begin();
  Serial.print("HTTP server started");
}

void loop() {
  server.handleClient();
}
