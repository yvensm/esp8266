#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <MFRC522.h>
#include <SPI.h>
#include <FS.h>
#include <string.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <ESP8266WebServer.h>

#define SS_PIN 15 //pin d8
#define RST_PIN 4//pin d2
#define buzzer 10 //pin sdd3
#define ledVerde 16 //pin d0
#define ledVermelho 5 //pin d1
#define porta 2 //pin d4
#define botao 0 //unused

const char *ssid = "ifce-alunos";
const char *password = "ifce4lun0";

const char *mqtt_server = "10.50.24.174";
const int mqtt_port = 1884;
const char *topic = "esp/test";

const char *http_server = "10.50.24.174";
const int http_port = 80;

WiFiClient espClient;
PubSubClient client(espClient);
ESP8266WebServer server;


MFRC522 reader(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key;

bool match = false;        //ID encontrado no storage
bool programMode = false;  //Modo programador
bool repaceMaster = false; //Substituir master
bool apMode= false;


uint8_t sucessRead; //Informa se houve sucesso na leitura

byte readID[4];   //ID Lido pelo leitor
byte storeID[4];  //ID Lido do storage
byte masterID[4]; //ID do master

//prototipos
void initSerial();
void initWiFi();
void initMQTT();
void reconnectMQTT();
void recconectWiFi();
void initRFIDReader();
void initPins();
void initSPI();
void initSPIFFS();
void initAPMode();
void callback(char *topic, byte *payload, unsigned int length);


uint8_t getID();
boolean checkTwo(byte a[], byte b[]);
boolean isMaster(byte master[]);
boolean findID(byte find[]);
boolean removeID(byte a[]);

void writeID(String a);
void writeID(byte a[]);
void concederAcesso();
void acessoNegado();
bool serverAction(byte uid[], uint8_t op);
void getAtt();
void rfidLoop();
void jsonFunc();


void setup() {
  initSerial();
  initSPI();
  initRFIDReader();
  initSPIFFS();
  //SPIFFS.format();
  initPins();
  initWiFi();
  initMQTT();

  server.on("/",[](){server.send(200,"text/plain","Hello World");});
  server.begin();
}
void loop() {
  
  rfidLoop();
  delay(200);
}
void rfidLoop(){
  do{
    if(!apMode){
      recconectWiFi();
      if(!client.connected()){
        reconnectMQTT();
      }
    }
    server.handleClient();
    Serial.println("Procurando tag...");
    sucessRead = getID();
    if(programMode){
      getAtt();
    }
    client.loop();
    delay(200);
  }while(!sucessRead);
  if (findID(readID)){
    Serial.println(F("Id cadastrado, acesso concedido"));
    concederAcesso();
  }
  else{
    Serial.println(F("ID não cadastrado, acesso recusado"));
    acessoNegado();
  }

  Serial.println(F("--------------------------------------"));
  delay(200);
}

void getAtt(){
  Serial.println("Atualizando...");
  jsonFunc();
  programMode = false;
}
void initRFIDReader(){
  reader.PCD_Init();
}

void initSPI(){
  SPI.begin();
}

void initPins(){
  pinMode(ledVerde, OUTPUT);
  pinMode(ledVermelho, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(botao, INPUT);
  pinMode(porta, OUTPUT);

  digitalWrite(ledVermelho, HIGH);
  digitalWrite(ledVerde, HIGH);
  digitalWrite(buzzer, LOW);
  digitalWrite(porta, HIGH);
}

void initSPIFFS(){
  SPIFFS.begin();
}

void initSerial(){
  Serial.begin(9600);
}

void initWiFi(){
  delay(10);
  Serial.println("Conectando-se em: " + String(ssid));
  
  WiFi.begin(ssid,password);
  for(int i=0;i<50;i++){
    if(WiFi.status() != WL_CONNECTED){
      delay(100);
      Serial.print(".");
    }
    else{
      Serial.print("Conectado na Rede " + String(ssid) + " | IP => ");
      Serial.println(WiFi.localIP());
      return;
    }
  }
  initAPMode();
  
}

void initMQTT(){
  client.setServer(mqtt_server,mqtt_port);
  client.setCallback(callback);
}

void reconnectMQTT() {
  if(WiFi.status() == WL_CONNECTED){
    while (!client.connected()) {
      Serial.println("Tentando se conectar ao Broker MQTT: " + String(mqtt_server));
      if (client.connect("ESP8266-ESP12-E")) {
        Serial.println("Conectado");
        client.subscribe(topic);

      } else {
        Serial.println("Falha ao Reconectar");
        Serial.println("Tentando se reconectar em 2 segundos");
        delay(2000);
      }
    }
  }
}

void recconectWiFi() {
  for(int i=0;i<200;i++){
    if (WiFi.status() != WL_CONNECTED) {
      delay(100);
      Serial.print(".");
    }else{
      Serial.println("Connectado!");
      return;
    }
  }
  initAPMode();
  apMode=true;
}

void acessoNegado(){
  digitalWrite(ledVermelho, LOW);
  delay(700);
  digitalWrite(ledVermelho, HIGH);
}

void concederAcesso(){
  digitalWrite(ledVerde, LOW);
  delay(50);
  digitalWrite(porta, LOW);
  delay(10);
  digitalWrite(porta, HIGH);
  serverAction(readID, 1);
  delay(700);
  digitalWrite(ledVerde, HIGH);
}

boolean removeID(byte a[]){
  File ids;
  File newarq;
  ids = SPIFFS.open("data.txt", "r");
  newarq = SPIFFS.open("new.txt", "w");

  while (ids.available()){
    String line;
    line = ids.readStringUntil('\n');
    char cline[10];
    line.toCharArray(cline, 10);
    uint32_t number = strtoul(cline, NULL, 16);
    for (uint8_t i = 3; i < 255; i--){
      storeID[i] = number & 0xFF;
      number >>= 8;
    }

    if (!checkTwo(readID, storeID)){
      for (uint8_t i = 0; i < 4; i++)
      {
        newarq.print(readID[i] < 0x10 ? "0" : "");
        newarq.print(readID[i], HEX);
      }
      newarq.println("");
    }
  }

  SPIFFS.remove("data.txt");
  SPIFFS.rename("new.txt", "data.txt");

  //Pisca 3x vermelho
  for (int i=0; i < 3; i++)
  {
    digitalWrite(ledVermelho, LOW);
    delay(200);
    digitalWrite(ledVermelho, HIGH);
    delay(200);
  }

  return true;
}

boolean checkTwo(byte a[], byte b[]){
  if (a[0] != 0)
    match = true;

  for (uint8_t i = 0; i < 4; i++)
  {
    if (a[i] != b[i])
      match = false;
  }
  if (match)
    return true;
  else
    return false;
}

boolean isMaster(byte test[]){
  if (checkTwo(test, masterID))
  {
    return true;
  }
  else
    return false;
}

boolean findID(byte find[]){
  File ids;
  ids = SPIFFS.open("data.txt", "r");

  while (ids.available()){
    String line;
    line = ids.readStringUntil('\n');
    char cline[10];
    line.toCharArray(cline, 10);
    uint32_t number = strtoul(cline, NULL, 16);
    for (uint8_t i = 3; i < 255; i--){
      storeID[i] = number & 0xFF;
      number >>= 8;
    }
    Serial.println("Novo id lido do arq");

    if (checkTwo(find, storeID)){
      Serial.print(F("Storage: "));
      for (uint8_t i = 0; i < 4; i++){
        Serial.print(storeID[i] < 0x10 ? "0" : "");
        Serial.print(storeID[i], HEX);
      }
      Serial.println("\n");
      Serial.print(F("readID: "));
      for (uint8_t i = 0; i < 4; i++){
        Serial.print(readID[i] < 0x10 ? "0" : "");
        Serial.print(readID[i], HEX);
      }
      Serial.println("\n");
      Serial.println(F("UID encontrado."));
      return true;
      break;
    }
    Serial.print(F("Storage: "));
    for (uint8_t i = 0; i < 4; i++){
      Serial.print(storeID[i] < 0x10 ? "0" : "");
      Serial.print(storeID[i], HEX);
    }
    Serial.println("\n");
    Serial.print(F("readID: "));
    for (uint8_t i = 0; i < 4; i++){
      Serial.print(readID[i] < 0x10 ? "0" : "");
      Serial.print(readID[i], HEX);
    }
    Serial.println("\n");
    Serial.println(F("UID não encontrado."));
  }
  return false;
  ids.close();
}

void writeID(byte a[]){
  File data;

  data = SPIFFS.open("data.txt", "w");

  //if (!findID(a)){
    Serial.print("ID a ser gravado:");
    for (uint8_t i = 0; i < 4; i++)
    {
      data.print(readID[i] < 0x10 ? "0" : "");
      data.print(readID[i], HEX);

      Serial.print(storeID[i] < 0x10 ? "0" : "");
      Serial.print(storeID[i], HEX);
    }
  Serial.println();
  
  data.println("");
  //Pisca 3x verde
  for (int i = 0; i < 3; i++){
    digitalWrite(ledVerde, LOW);
    delay(200);
    digitalWrite(ledVerde, HIGH);
  
  }
  Serial.println("Novo id gravado com sucesso!");
  //serverAction(readID, 0);
  //}

  // else{
  //   //Algum padrão de led
  //   Serial.println("Gravação falhou, algo errado não está certo!");
  // }

  data.close();
}

void writeID(String a){
  File data;

  data = SPIFFS.open("data.txt", "a");

  //if (!findID(a)){
    Serial.print("ID a ser gravado:");
    /* for (uint8_t i = 0; i < 4; i++)
    {
      data.print(readID[i] < 0x10 ? "0" : "");
      data.print(readID[i], HEX);

      Serial.print(storeID[i] < 0x10 ? "0" : "");
      Serial.print(storeID[i], HEX);
    } */
  Serial.print(a);
  Serial.println();
  data.println(a);
  //data.println("");
  //Pisca 3x verde
  for (int i = 0; i < 3; i++){
    digitalWrite(ledVerde, LOW);
    delay(200);
    digitalWrite(ledVerde, HIGH);
  
  }
  Serial.println("Novo id gravado com sucesso!");
  //serverAction(readID, 0);
  //}

  // else{
  //   //Algum padrão de led
  //   Serial.println("Gravação falhou, algo errado não está certo!");
  // }

  data.close();
}

uint8_t getID(){
  //Procurando novo RFID
  if (!reader.PICC_IsNewCardPresent())
    return 0;
  //Verificando se o NUID foi lido
  if (!reader.PICC_ReadCardSerial())
    return 0;
  Serial.println(F("Scanned PICC's UID:"));
  for (uint8_t i = 0; i < 4; i++)
  { //
    readID[i] = reader.uid.uidByte[i];
    Serial.print(readID[i] < 0x10 ? "0" : "");
    Serial.print(readID[i], HEX);
  }
  Serial.println("");
  //buzinar
  for(int i=0;i<50;i++){
    digitalWrite(buzzer,HIGH);
    delay(1);
    digitalWrite(buzzer,LOW);
  }
  reader.PICC_HaltA(); // Stop reading
  return 1;
}

bool serverAction(byte uid[], uint8_t op){ // 0 insere novo usuario, 1 insere log

  byte idcript[4];
  for (uint8_t i = 0; i < 4; i++){
    idcript[i] = uid[i];
  }

  if(!espClient.connect(http_server, http_port))
  {
    Serial.println("Falha na conexao com o site ");
    return false;
  }

  String param = "?uid="; //Parâmetros com as leituras
  for(uint8_t i = 0; i < 4; i++)
  {
    param += idcript[i] < 0x10 ? "0" : "";
    param += String(idcript[i], HEX);
  }
  Serial.println(param);
  if(op == 0)
  {
    espClient.print("GET /esp/insert.php" + param + "\r\n" +
                    "Host: " + http_server + "Connection: close\r\n\r\n");
  }
  else if (op == 1)
  {
    espClient.print("GET /esp/log.php" + param + "\r\n" +
                    "Host: " + http_server + "Connection: close\r\n\r\n");
  }
  else if (op == 2)
  {
    //Remover usuario
  }
  /*espClient.println("Host: ");
    espClient.println(http_server);
    espClient.println("Connection: close\r\n\r\n");
    espClient.println();
    espClient.println(); */

  // Informações de retorno do servidor para debug
  /* while(espClient.available()){
    String line = espClient.readStringUntil('\r');
    Serial.print(line);
  } */
  return true;
}

void jsonFunc(){
  Serial.println("Trying to connect to the server.");
  uint32_t id = system_get_chip_id();
  Serial.print("Chip id:");
  Serial.println(id);
  if(WiFi.status() == WL_CONNECTED){
      int tam = 0;
      HTTPClient http;
      http.begin("http://"+String(http_server)+"/TrancasWebapp/server/Controller/jsonRequest.php?op=0");
      int httpCode = http.GET();
      
      if(httpCode > 0){
        Serial.println("Server Mensage Receved.");
        const size_t bufferSize = JSON_OBJECT_SIZE(1) + 30;
        DynamicJsonBuffer jsonBuffer(bufferSize);
        JsonObject& root = jsonBuffer.parseObject(http.getString());

        tam = root["tam"];
        // Output to serial monitor
        Serial.print("Tam =");
        Serial.println(tam); 
        delay(1000);
        
        http.end();
      }
      http.begin("http://"+String(http_server)+"/TrancasWebapp/server/Controller/jsonRequest.php?op=1");
      httpCode = http.GET();

      if(httpCode > 0){
        Serial.println();
        const size_t bufferSize = JSON_ARRAY_SIZE(tam) + JSON_OBJECT_SIZE(1) + (tam+1)*10;
        DynamicJsonBuffer jsonBuffer(bufferSize);
        JsonObject& root = jsonBuffer.parseObject(http.getString());
        Serial.println("IDs Recebidos: ");


        //byte readUID[4];
        //char cline[10];
        

        for(int i=0;i<tam;i++){
        String line = root["uid"][i];
        //  line.toCharArray(cline, 10);
        //  uint32_t number = strtoul(cline, NULL, 16);
        //  for (uint8_t i = 3; i < 255; i--){
        //    readUID[i] = number & 0xFF;
        //    number >>= 8;
        //}
          writeID(line);
          //Serial.print("Number: ");
          //Serial.println(number);
          Serial.println(line);
        }
      }
  }
  //delay(1000);
}

void callback(char *topic, byte *payload, unsigned int length){
  const char *msg = "att1010";
  const char *msg2= "eeeeeee1";
  const int msgTam = 7;
  const int msg2Tam = 8;

  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  bool equal=true;
  bool equal2=true;
  if(length!=msgTam){
      
      equal=false;
  }

  for (int i = 0; i < length; i++){
    Serial.print((char)payload[i]);
    if((char)payload[i]!=msg[i]){
      
      equal=false;
    }
  }

  //Gambiarra para aceitar uid pelo mqtt
  if(length != msg2Tam){
    equal2=false;
  }
  for (int i = 0; i < length; i++){
    //Serial.print((char)payload[i]);
    if((char)payload[i]!=msg2[i]){
      
      equal2=false;
    }
  }
  if(equal2){
    reader.PICC_HaltA();
    char cline[8];
    for(int i=0;i<8;i++){
      cline[i]=(char)payload[i];
    }
    Serial.print("Id recebido do mqtt:");
    Serial.println(cline);
    uint32_t number = strtoul(cline, NULL, 16);
    for (uint8_t i = 3; i < 255; i--){
      readID[i] = number & 0xFF;
      number >>= 8;
    }
    sucessRead=1;
    Serial.printf(" ESP8266 Chip id = %08X\n", ESP.getChipId());  
  }


  if(equal){
    Serial.println("Entrando em modo atualizacao!");
    delay(300);
    programMode=true;
  }
 

  Serial.println();
  Serial.println("-----------------------");

}

void initAPMode(){
  char espName[20];
  sprintf(espName,"ESP-%08X\n", ESP.getChipId());
  Serial.println(espName);
  apMode=true;
  WiFi.mode(WIFI_AP_STA);
  bool result = WiFi.softAP(espName);
  if(result){
    Serial.println(WiFi.softAPIP());
  }
}