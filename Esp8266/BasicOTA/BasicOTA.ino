#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <MFRC522.h>
#include <SPI.h>

//RFID CONFIG
#define SS_PIN 15
#define RST_PIN 4

const char* ssid = "ifce-alunos";
const char* password = "ifce4lun0s";

const char* apname = "Esp8266";
const char* appw = "asd12345";

//Objeto "chave" para autenticação
MFRC522::MIFARE_Key key;
//Status de retorno de autenticação
MFRC522::StatusCode status;
//Definições pino modulo rc522
MFRC522 mfrc522(SS_PIN,RST_PIN);

byte nuid[4];
char* nome;

ESP8266WebServer server(80);


void paginaStart(){
   String buf = "<html><head></head>";
   buf += "<body>";
   buf += "Hello World";
   buf += "</body></html>";
   server.send(200,"text/html",buf);
}


void startAP(){
  Serial.println("Starting Access Point...");
  WiFi.softAP(apname,appw);
  IPAddress myip = WiFi.softAPIP();
  Serial.println("AP IP Address: ");
  Serial.println(myip);
  server.on("/",paginaStart);  
  server.begin();
  Serial.println("HTTP server started");
  server.close();
  wifiSetup();
  
}

void otaSetup(){
  ArduinoOTA.setHostname("myesp8266");

  ArduinoOTA.setPassword("asd123");

  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");
  ArduinoOTA.onStart([]() {
         Serial.println("\nEnd");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void wifiSetup(){
   
   WiFi.mode(WIFI_STA);
   WiFi.begin(ssid, password);
  
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(2000);
    //startAP();
  }else{
    Serial.println("Connected...");
    otaSetup();
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  SPI.begin(); //Inicia SPI bus
  Serial.println("SPI OK");
  //pinMode(pinVerde,OUTPUT);
  //pinMode(pinVermelho,OUTPUT);
  //inicia MFRC522
  
  mfrc522.PCD_Init();
  wifiSetup();
  
  Serial.println("Aproxime o seu cartao do leitor...");
  pinMode(D1,OUTPUT);
}

void loop() {
  ArduinoOTA.handle();
  //server.handleClient();
  delay(0);
  if(! mfrc522.PICC_IsNewCardPresent()){
      return;
    }

  if(! mfrc522.PICC_ReadCardSerial()){
    return;
  }

  readUID();
  //fala pro picc quando no estato ACTIVE a ir para um estado de "parada"
  mfrc522.PICC_HaltA();
  //stop a encrptação do pcd, deve ser chamado após a comunicação com
  //autenticação,caso contrário novas comunicações não poderão ser iniciadas
  mfrc522.PCD_StopCrypto1();

}

void printHex(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

/**
 * Helper routine to dump a byte array as dec values to Serial.
 */
void printDec(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}

void readUID(){
  Serial.println(F("PICC type: "));
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  if(piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
   piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
   piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
      Serial.println(F("Your tag is not of type MIFARE Classic."));
      return;
  }
  if (mfrc522.uid.uidByte[0] != nuid[0] || 
    mfrc522.uid.uidByte[1] != nuid[1] || 
    mfrc522.uid.uidByte[2] != nuid[2] || 
    mfrc522.uid.uidByte[3] != nuid[3] ) {
    Serial.println(F("A new card has been detected."));

    // Store NUID into nuid array
    for (byte i = 0; i < 4; i++) {
      nuid[i] = mfrc522.uid.uidByte[i];
    }
    nome="Yvens";
    Serial.println(F("The NUID tag is:"));
    Serial.print(F("In hex: "));
    printHex(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("In dec: "));
    printDec(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
  }
  else {
    Serial.println(F("Card read previously."));
    Serial.print(F("Bem vindo "));
    Serial.println(nome);
  }
}

